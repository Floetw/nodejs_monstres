const express = require ('express');
const app = express();
const port = process.env.PORT || 4000;
const monsterRouter = require('./routes/monstreRoute');
const newRouter = require('./routes/new');

// CONFIG APPLI
app.use(express.static(__dirname +'/public'));
app.set('view engine', 'pug');



// MES ROUTES
app.get('/', function(req, res){
    res.render('home')
})
app.use('/monstres', monsterRouter);
app.use('/', newRouter);


app.listen(port, function() {
    console.log('[APPLICATION IS RUNNING!]')
})