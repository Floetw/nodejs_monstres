const express = require ('express');
const monsterRouter = express.Router();
const monstres = require('../monstres');

// La route GET/all pour voir tous les monstres
monsterRouter.get('/all', function(req, res){
    res.render('monstres', {monstres: monstres}) // 1 : pug, 2 : ancre pour le fichier pug, 3 : js
})

// La route GET/:id pour voir un monstre
monsterRouter.get('/:id', function(req, res){
    const monstre = monstres[req.params.id -1];
    res.render('monstre', {monstre:monstre})
})

module.exports = monsterRouter;